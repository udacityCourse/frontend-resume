/*
This is empty on purpose! Your code to build the resume will go here.
 */
 


var bio = {
	"name" : "ALI BAZROUN",
	"role" : "Software Engineer",
	"contactNo": {
		"mobile" : "0545586246",
		"email": "bazroun.000@gmail.com",
		"webpage" : "bazroun.netau.net"
	},
	"pictureURL" : "images/personal photo.jpg",
	"welcomMSG" : "Welcome to my Personnal Resume &#9786",
	"skills" : ["skill1<br>", "skill2<br>", "skill3<br>"]
}

var newName= HTMLheaderName.replace("%data%", bio.name);
$("#header").prepend(newName);

var newRole= HTMLheaderRole.replace("%data%", bio.role);
$("#header").prepend(newRole);

var mob= HTMLmobile.replace("%data%", bio.contactNo.mobile);
$("#topContacts").append(mob);

var myEmail= HTMLemail.replace("%data%", bio.contactNo.email);
$("#topContacts").append(myEmail);

var web= HTMLwebpage.replace("%data%", bio.contactNo.webpage);
$("#topContacts").append(web);

var pic= HTMLbioPic.replace("%data%", bio.pictureURL);
$("#header").append(pic);

var wlcmMsg= HTMLwelcomeMsg.replace("%data%", bio.welcomMSG);
$("#header").append(wlcmMsg);

var skillTitle= HTMLskillsStart;
$("#header").append(skillTitle);

var mySkill= HTMLskills.replace("%data%", bio.skills);
$("#header").append(mySkill);


/**
	EDUCATION
*/

var edu = {
	"uni" : "King Fahd University of Petroleum and Minerals (KFUPM)",
	"degree" : "Bachelor Degree in Computer Scince",
	"date" : "2010 - 2015",
	"location" : "Saudi Arabia, Dhahran",
	"Major" : "Software Engineering"
}

var univ= HTMLschoolStart;
$("#education").append(univ);

var myUni= HTMLschoolName.replace("%data%", edu["uni"]);
$("#education").append(myUni);

var myDeg= HTMLschoolDegree.replace("%data%", edu["degree"]);
$("#education").append(myDeg);

var uniDate= HTMLschoolDates.replace("%data%", edu["date"]);
$("#education").append(uniDate);


/**
	WORK EXPERIENCE
*/

var work = {
	"job1" : {
		"employer" : "GISTIC",
		"title" : "Software Development Engineer",
		"date" : "21/7/2015 - Upto Now",
		"location": "Saudi Arabia, Mecca",
		"descrip" : "Analyze and Write Code in Different Programming Languages."
	},
	"job2" : {
		"employer" : "Oxygen_ME",
		"title" : "Software/System Engineer",
		"date" : "10/5/2014 - 21/7/2015",
		"location": "Saudi Arabia, Khubar",
		"descrip" : "Analyze Networks Issues and find the optimal solution."
	} 
	
}

var workStrt = HTMLworkStart;
$("#workExperience").append(workStrt);

/**
	JOB #1
*/

var emp= HTMLworkEmployer.replace("%data%", work.job1.employer);
$("#workExperience").append(emp);

var ttl= HTMLworkTitle.replace("%data%", work.job1.title);
$("#workExperience").append(ttl);

var dat= HTMLworkDates.replace("%data%", work.job1.date);
$("#workExperience").append(dat);

var loc= HTMLworkLocation.replace("%data%", work.job1.location);
$("#workExperience").append(loc);

var desc= HTMLworkDescription.replace("%data%", work.job1.descrip);
$("#workExperience").append(desc);

/**
	JOB #2
*/

var workStrt = HTMLworkStart;
$("#workExperience").append(workStrt);

var emp2= HTMLworkEmployer.replace("%data%", work.job2.employer);
$("#workExperience").append(emp2);

var ttl2= HTMLworkTitle.replace("%data%", work.job2.title);
$("#workExperience").append(ttl2);

var dat2= HTMLworkDates.replace("%data%", work.job2.date);
$("#workExperience").append(dat2);

var loc2= HTMLworkLocation.replace("%data%", work.job2.location);
$("#workExperience").append(loc2);

var desc2= HTMLworkDescription.replace("%data%", work.job2.descrip);
$("#workExperience").append(desc2);


/**
	PROJECTS
*/
var projects = {
	"pro1" : {
		"title" : "Senior Project: Space Invasion Game",
		"date" : "2015",
		"descrip" : "Space Invasion Game is an Android 2D Game that has 5 Stages with 10 Different Types of Enemies and 3 Unique Spaceships to Play by.",
		"images" : "images/space invasion-small.png"
	},
	"pro2" : {
		"title" : "ppp222",
		"date" : "pp222ddd",
		"descrip" : "ppp222scscsc",
		"images" : "images/space invasion-small.png"
	}
}

				//PROJECT1
var projectStrt = HTMLprojectStart;
$("#projects").append(projectStrt);

var title1= HTMLprojectTitle.replace("%data%", projects.pro1.title);
$("#projects").append(title1);

var date1= HTMLprojectDates.replace("%data%", projects.pro1.date);
$("#projects").append(date1);

var descrip1= HTMLprojectDescription.replace("%data%", projects.pro1.descrip);
$("#projects").append(descrip1);

var images1= HTMLprojectImage.replace("%data%", projects.pro1.images);
$("#projects").append(images1);

				//PROJECT2

var title2= HTMLprojectTitle.replace("%data%", projects.pro2.title);
$("#projects").append(title2);

var date2= HTMLprojectDates.replace("%data%", projects.pro2.date);
$("#projects").append(date2);

var descrip2= HTMLprojectDescription.replace("%data%", projects.pro2.descrip);
$("#projects").append(descrip2);

var images2= HTMLprojectImage.replace("%data%", projects.pro2.images);
$("#projects").append(images2);
